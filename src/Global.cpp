#include "Global.hpp"

#include <ctime>
#include <random>

namespace global
{

std::mt19937 mt(std::time(nullptr));
sf::Vector2u window_size(740, 900);
thor::ResourceHolder<sf::Texture, Asset> resource_holder;

std::array<const std::string, static_cast<unsigned>(Asset::max_assets)> file_paths
{
	"img/menu_background.png",
	"img/poker.cards.bypx.png",
	"img/card_back.png",
	"img/card_joker.bmp",
	"img/stack_empty3.png",
	"img/stack_empty2.png"
};

const std::string& getAssetPath(Asset name)
{
	return file_paths[static_cast<unsigned>(name)];
}

} // namespace global

namespace render_settings
{
	float scale; ///< @brief scale at which cards are drawn; 1.0 means no scaling
	sf::Vector2f card_scaled_size;
	sf::Vector2f card_space;
	sf::Vector2f card_slide;
}
