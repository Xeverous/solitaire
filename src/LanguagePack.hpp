#ifndef LANGUAGEPACK_H
#define LANGUAGEPACK_H
#include <string>
#include <array>
#include <fstream>

class LanguagePack
{
public:
	LanguagePack();
	~LanguagePack() = default;

	enum class name_type
	{
		GAME_NAME = 0,
		PLAY,
		EXIT,
		CONTINUE,
		RUN,
		BACK_TO_MENU,
		RETURN,
		SOLITAIRE,
		SPIDER_1,
		SPIDER_2,
		SPIDER_4,
		ALWAYS_LAST_ENUM
	};

	enum class Language
	{
		EN,
		PL,
		Default = EN
	};

	const std::string& getName(LanguagePack::name_type name) const;

private:
	Language language;

	std::array<
		std::string,
		static_cast<unsigned>(name_type::ALWAYS_LAST_ENUM)> text_data;
};

#endif // LANGUAGEPACK_H
