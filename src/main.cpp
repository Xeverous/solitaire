#include "Application.hpp"

int main()
{
    auto app = std::make_unique<Application>();
    int exit_code = app->run();

    return exit_code;
}
