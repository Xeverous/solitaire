#ifndef APPLICATION_CPP
#define APPLICATION_CPP
#include "Application.hpp"

#include <SFML/Graphics.hpp>
#include <Aurora/Config.hpp>
#include <iostream>
#include <deque>
#include <fstream>
#include <string>
#include <cstdlib>
#include <exception>

#include "game/Solitaire.hpp"
#include "game/Spider.hpp"
#include "Global.hpp"
#include "LanguagePack.hpp"
#include "UserInterface.hpp"

Application::Application()
	: ui(*this)
{

}

Application::~Application()
{
	if (game)
		game->save();
}

void Application::initSettingConfig()
{
	// TODO load config
}

void Application::initLog()
{
	// TODO implement logging
}

void Application::initRender()
{
	sf::VideoMode desktop = sf::VideoMode::getDesktopMode();
	sf::ContextSettings settings;
	settings.antialiasingLevel = 8;

	window.create(
	    sf::VideoMode(global::window_size.x, global::window_size.y, desktop.bitsPerPixel),
	    lp.getName(LanguagePack::name_type::GAME_NAME),
	    sf::Style::Default,
	    settings);

	window.setVerticalSyncEnabled(true);
}

void Application::initAssets()
{
	background_texture.loadFromFile(global::getAssetPath(global::Asset::background));
	background_texture.setRepeated(true);
	background_sprite.setTexture(background_texture);
	background_sprite.setTextureRect(sf::IntRect(0, 0, global::window_size.x, global::window_size.y));
	Card::loadAssets();
	Stack::loadAssets();
}

CardGame* Application::getGame() const
{
	return game.get();
}

int Application::run()
{
	try
	{
		initLog();
		initSettingConfig();
		initRender();
		initAssets();

		gameLoop();
	}
	catch (const std::exception& e)
	{
		std::cout << e.what() << '\n';
		return 1;
	}

	return 0;
}

void Application::gameLoop()
{
	sf::Clock clock;

	while (window.isOpen())
	{
		update(clock.restart());

		sf::Event event;
		while (window.pollEvent(event))
			handleEvent(event);

		window.clear();
		draw();
		window.display();
	}
}

void Application::update(sf::Time time)
{
	ui.update(time);

	if (game)
		game->update(time);
}

void Application::handleEvent(const sf::Event& event)
{
	if (event.type == sf::Event::Closed)
	{
		window.close();
	}
	else if (event.type == sf::Event::Resized)
	{
		global::window_size.x = event.size.width;
		global::window_size.y = event.size.height;
		sf::View view(sf::Vector2f(global::window_size.x / 2, global::window_size.y / 2), sf::Vector2f(global::window_size.x, global::window_size.y));
		window.setView(view);

		background_sprite.setTextureRect(sf::IntRect(0, 0, global::window_size.x, global::window_size.y));

		if (game)
			game->reinitRender();
	}
	// @todo else if (event.type == sf::Event::LostFocus)
	// @todo else if (event.type == sf::Event::GainedFocus)

	sf::Vector2f mouse_position = window.mapPixelToCoords(sf::Mouse::getPosition(window));
	ui.handleEvent(event, mouse_position);

	if (game)
		game->handleEvent(event, mouse_position);
}

void Application::draw()
{
	window.draw(background_sprite);

	if (game)
		game->draw(window);

	ui.draw(window);
}

void Application::createGame(CardGame::GameType game_type)
{
	if (game_type == CardGame::GameType::Solitaire)
		game = std::make_unique<Solitaire>();
	else
		game = std::make_unique<Spider>(1); // TODO pick up settings from GUI
}

#endif // APPLICATION_CPP
