#ifndef LANGUAGEPACK_CPP
#define LANGUAGEPACK_CPP
#include "LanguagePack.hpp"

#include <string>
#include <fstream>

LanguagePack::LanguagePack()
	: language(Language::Default)
{
	std::ifstream file("data/language");
	std::string config_language;
	std::getline(file, config_language);

	if (config_language == "EN")
		language = Language::EN;
	else
		language = Language::PL;

	file.close();

	std::string s = "data/" + config_language;
	file.open(s.c_str());

	for (unsigned i = 0; i < text_data.size(); i++)
		std::getline(file, text_data[i]);
}

const std::string& LanguagePack::getName(LanguagePack::name_type name) const
{
	return text_data[static_cast<unsigned>(name)];
}

#endif // LANGUAGEPACK_H
