#include "UserInterface.hpp"

#include "Application.hpp"
#include "Global.hpp"

UserInterface::UserInterface(Application& application)
	: application(application),
	  FPS(0)
{
	m_window = sfg::Window::Create(sfg::Window::Style::TITLEBAR | sfg::Window::Style::BACKGROUND);
	m_window->SetTitle("Menu");
	desktop.Add(m_window);

	m_notebook = sfg::Notebook::Create();
	m_box_main = sfg::Box::Create(sfg::Box::Orientation::VERTICAL, 5.f); //V
		m_box_top = sfg::Box::Create(sfg::Box::Orientation::HORIZONTAL, 5.f); //H
			m_frame_new_game = sfg::Frame::Create("New Game");
				m_box_new_game = sfg::Box::Create(sfg::Box::Orientation::VERTICAL, 5.f); //V
					m_button_solitaire = sfg::Button::Create("Solitaire");
					m_button_spider = sfg::Button::Create("Spider");
			m_frame_current_game = sfg::Frame::Create("Current Game");
				m_box_current_game = sfg::Box::Create(sfg::Box::Orientation::VERTICAL, 5.f); //V
					m_button_resume = sfg::Button::Create("resume");
					m_button_restart = sfg::Button::Create("restart");
					m_button_save = sfg::Button::Create("save");
					m_button_load = sfg::Button::Create("load");
		m_box_bot = sfg::Box::Create(sfg::Box::Orientation::HORIZONTAL, 5.f); //H
			m_frame_options_general = sfg::Frame::Create("General Options");
				m_box_options_general = sfg::Box::Create(sfg::Box::Orientation::VERTICAL, 5.f); //V
					m_cbutton_show_fps = sfg::CheckButton::Create("show FPS");
					m_cbutton_vsync = sfg::CheckButton::Create("VSync");
					m_cbutton_save = sfg::CheckButton::Create("Save game on exiting");
					m_cbutton_load = sfg::CheckButton::Create("Load game on startup");
			//m_frame_options_solitaire = sfg::Frame::Create("Solitaire Options");
			m_box_main_settings = sfg::Box::Create(sfg::Box::Orientation::VERTICAL, 5.f); //V
				m_frame_options_solitaire = sfg::Frame::Create("Solitaire");
					m_box_options_solitaire = sfg::Box::Create(sfg::Box::Orientation::VERTICAL, 5.f); //V
						//m_rbutton_draw1 = sfg::RadioButton::Create("draw 1");
						//m_rbutton_draw3 = sfg::RadioButton::Create("draw 3", m_rbutton_draw1->GetGroup());
						m_cbutton_show_score = sfg::CheckButton::Create("show score");
						m_cbutton_time_penalty = sfg::CheckButton::Create("apply time penalty");
				m_frame_options_spider = sfg::Frame::Create("Spider");
					m_box_options_spider = sfg::Box::Create(sfg::Box::Orientation::VERTICAL, 5.f); //V
						m_rbutton_colors1 = sfg::RadioButton::Create("1 color");
						m_rbutton_colors2 = sfg::RadioButton::Create("2 colors", m_rbutton_colors1->GetGroup());
						m_rbutton_colors4 = sfg::RadioButton::Create("4 colors", m_rbutton_colors1->GetGroup());
				m_table_graphic_settings = sfg::Table::Create();
					m_scale_space = sfg::Scale::Create(0.f,  0.5f, 0.01f);	m_label_space = sfg::Label::Create("0");
					m_scale_slide = sfg::Scale::Create(0.f, 0.25f, 0.01f);	m_label_slide = sfg::Label::Create("0");

	m_button_back = sfg::Button::Create("Menu");

	m_window->Add(m_notebook);
	m_notebook->AppendPage(m_box_main, sfg::Label::Create("  Main  "));
		m_box_main->Pack(m_box_top, false, false);
			m_box_top->Pack(m_frame_new_game);
				m_frame_new_game->Add(m_box_new_game);
					m_box_new_game->Pack(m_button_solitaire, true, true);
					m_box_new_game->Pack(m_button_spider, true, true);
			m_box_top->Pack(m_frame_current_game, false, false);
				m_frame_current_game->Add(m_box_current_game);
					m_box_current_game->Pack(m_button_resume,  true, true);
					m_box_current_game->Pack(m_button_restart, true, true);
					m_box_current_game->Pack(m_button_save,    true, true);
					m_box_current_game->Pack(m_button_load,    true, true);
		m_box_main->Pack(m_box_bot, false, false);
			m_box_bot->Pack(m_frame_options_general,   false, false);
				m_frame_options_general->Add(m_box_options_general);
					m_box_options_general->Pack(m_cbutton_show_fps, true, true);
					m_box_options_general->Pack(m_cbutton_vsync, true, true);
					m_box_options_general->Pack(m_cbutton_save, true, true);
					m_box_options_general->Pack(m_cbutton_load, true, true);

	m_notebook->AppendPage(m_box_main_settings, sfg::Label::Create("Settings"));
		m_box_main_settings->Pack(m_frame_options_solitaire, false, false);
			m_frame_options_solitaire->Add(m_box_options_solitaire);
				//m_box_bot->Pack(m_frame_options_solitaire, false, false);
				//	m_frame_options_solitaire->Add(m_box_options_solitaire);
				//m_box_options_solitaire->Pack(m_rbutton_draw1, true, true);
				//m_box_options_solitaire->Pack(m_rbutton_draw3, true, true);
				m_box_options_solitaire->Pack(m_cbutton_show_score,   false, false);
				m_box_options_solitaire->Pack(m_cbutton_time_penalty, false, false);
		m_box_main_settings->Pack(m_frame_options_spider, false, false);
			m_frame_options_spider->Add(m_box_options_spider);
				m_box_options_spider->Pack(m_rbutton_colors1, false, false);
				m_box_options_spider->Pack(m_rbutton_colors2, false, false);
				m_box_options_spider->Pack(m_rbutton_colors4, false, false);
		m_box_main_settings->Pack(m_table_graphic_settings, true, true);
			m_table_graphic_settings->Attach( sfg::Label::Create("Space between cards"), 	sf::Rect<sf::Uint32>( 0, 0, 1, 1 ), sfg::Table::FILL, sfg::Table::FILL);
			m_table_graphic_settings->Attach( m_scale_space, 					            sf::Rect<sf::Uint32>( 1, 0, 1, 1 ), sfg::Table::FILL, sfg::Table::FILL);
			m_table_graphic_settings->Attach( m_label_space, 					            sf::Rect<sf::Uint32>( 2, 0, 1, 1 ), sfg::Table::FILL, sfg::Table::FILL);
			m_table_graphic_settings->Attach( sfg::Label::Create("Slide of stacked cards"), sf::Rect<sf::Uint32>( 0, 1, 1, 1 ), sfg::Table::FILL, sfg::Table::FILL);
			m_table_graphic_settings->Attach( m_scale_slide, 					            sf::Rect<sf::Uint32>( 1, 1, 1, 1 ), sfg::Table::FILL, sfg::Table::FILL);
			m_table_graphic_settings->Attach( m_label_slide, 					            sf::Rect<sf::Uint32>( 2, 1, 1, 1 ), sfg::Table::FILL, sfg::Table::FILL);

	// Menu button
	m_button_back->GetSignal( sfg::Button::OnLeftClick ).Connect(std::bind(&UserInterface::maximizeUI, this));
	// New Game
	m_button_solitaire->GetSignal( sfg::Button::OnLeftClick ).Connect(std::bind(&UserInterface::clickedNewGame, this, CardGame::GameType::Solitaire));
	m_button_spider   ->GetSignal( sfg::Button::OnLeftClick ).Connect(std::bind(&UserInterface::clickedNewGame, this, CardGame::GameType::Spider));
	// Current Game
	m_button_resume ->GetSignal( sfg::Button::OnLeftClick ).Connect(std::bind(&UserInterface::minimizeUI, this));
	m_button_restart->GetSignal( sfg::Button::OnLeftClick ).Connect(std::bind(&UserInterface::clickedRestart, this));
	m_button_save   ->GetSignal( sfg::Button::OnLeftClick ).Connect(std::bind(&UserInterface::clickedSave, this));
	m_button_load   ->GetSignal( sfg::Button::OnLeftClick ).Connect(std::bind(&UserInterface::clickedLoad, this));
	// Render Settings
	m_scale_space->GetSignal( sfg::Scale::OnMouseLeftRelease ).Connect(std::bind(&UserInterface::changedRenderSettings, this));
	m_scale_slide->GetSignal( sfg::Scale::OnMouseLeftRelease ).Connect(std::bind(&UserInterface::changedRenderSettings, this));

	//setting (should be done by config)
	m_rbutton_colors1->SetActive(true);
	//m_rbutton_draw1->setActive(true);
	m_cbutton_show_fps->SetActive(true);

	m_window->SetPosition(sf::Vector2f(
		(global::window_size.x - m_window->GetAllocation().width ) / 2,
		(global::window_size.y - m_window->GetAllocation().height) / 2));

	m_window_mini = sfg::Window::Create();
	m_window_mini->Show(false);
	m_window_mini->Add(m_button_back);
	desktop.Add(m_window_mini);

	m_button_back->SetRequisition(sf::Vector2f(50.f, 50.f));
	m_button_back->SetPosition(sf::Vector2f(0.f, global::window_size.y - 50));

	m_scale_space->SetRequisition({100, 25});
	m_scale_slide->SetRequisition({100, 25});

	if (!m_render_texture.create(2000, 500))
		std::cout << "cant create render texture\n";
	m_render_texture.clear(sf::Color(255, 0, 0));
	m_sprite.setTexture(m_render_texture.getTexture());
	m_sprite.setPosition(sf::Vector2f(0, global::window_size.y / 2));

	if (!FPS_font.loadFromFile("fonts/SourceSansPro-Semibold.otf"))
		std::cout << "\nFailed to load font: SourceSansPro-Semibold.otf";
	FPS_text.setFont(FPS_font);
	FPS_text.setFillColor(sf::Color::Yellow);
	FPS_text.setPosition(0, -4);
	FPS_text.setCharacterSize(15);

	loadConfig();
	changedRenderSettings();
	maximizeUI();
}

void UserInterface::loadConfig()
{
	float space, slide;
	std::ifstream file("data/config");
	file >> space;
	file >> slide;

	if (space < 0.0f or space > 1.0f)
		space = 0.11f;

	if (slide < 0.0f or slide > 1.0f)
		slide = 0.15f;

	m_scale_space->SetValue(space);
	m_scale_slide->SetValue(slide);
}

void UserInterface::minimizeUI()
{
	maximized = false;
	m_window->Show(maximized);
	m_window_mini->Show(!maximized);

	auto game = application.getGame();
	if (game != nullptr)
		game->resume();
}

void UserInterface::maximizeUI()
{
	maximized = true;
	m_window->Show(maximized);
	m_window_mini->Show(!maximized);

	auto game = application.getGame();
	if (game != nullptr)
		game->pause();
}

void UserInterface::handleEvent(const sf::Event& event, sf::Vector2f mouse_position)
{
	if (event.type == sf::Event::KeyPressed)
	{
		if (event.key.code == sf::Keyboard::Escape)
		{
			if (maximized)
				minimizeUI();
			else
				maximizeUI();

			return;
		}
	}

	if (maximized)
		m_window->HandleEvent(event);
	else
		m_button_back->HandleEvent(event);

	if (event.type == sf::Event::Resized)
		m_button_back->SetPosition(sf::Vector2f(0.f, event.size.height - 50));

	// no use so far
	(void)mouse_position;
}

void UserInterface::update(sf::Time time)
{
	if (maximized)
		m_window->Update(time.asSeconds());
	else
		m_button_back->Update(time.asSeconds());

	FPS = 1.0f / time.asSeconds();
	FPS_to_draw = "FPS: " + std::to_string(FPS);
	FPS_text.setString(FPS_to_draw);
}

void UserInterface::draw(sf::RenderWindow& window)
{
	m_sfgui.Display(window);

	if (m_cbutton_show_fps->IsActive())
		window.draw(FPS_text);
}

//RESIZING
//	Tell SFGUI how big it should be
//	m_window->SetRequisition(sf::Vector2f(x, y));
//
//	// Tell SFGUI to reset the current size and position
//	m_window->SetAllocation(sf::FloatRect(0, 0, 0, 0));
//
//	// Tell SFGUI to recalculate
//	desktop.Refresh();

void UserInterface::clickedRestart()
{
	CardGame* game = application.getGame();
	if (game != nullptr)
		game->restart();
}

void UserInterface::clickedSave()
{
	CardGame* game = application.getGame();
	if (game != nullptr)
		game->save();
}

void UserInterface::clickedLoad()
{
	CardGame* game = application.getGame();
	if (game != nullptr)
		game->load();
}

std::string UserInterface::scaleToString(float value)
{
	if (value > 1.0f or value < 0.0f)
		return "?";

	value *= 100.0f;

	std::array<char, 5> buf;
	std::sprintf(buf.data(), "%.0f%%", value);
	return buf.data();
}

void UserInterface::changedRenderSettings()
{
	m_label_space->SetText(scaleToString(m_scale_space->GetValue()));
	m_label_slide->SetText(scaleToString(m_scale_slide->GetValue()));

	CardGame* game = application.getGame();
	if (game)
	{
		CardGame::CARD_SPACE_FACTOR = m_scale_space->GetValue();
		CardGame::CARD_SLIDE_FACTOR = m_scale_slide->GetValue();
		game->reinitRender();
	}
}

void UserInterface::clickedNewGame(CardGame::GameType game_type)
{
	application.createGame(game_type);
	changedRenderSettings();
	minimizeUI();
}

void UserInterface::setActive(bool status)
{
	if (status)
		maximizeUI();
	else
		minimizeUI();
}
