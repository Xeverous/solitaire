#ifndef USERINTERFACE_HPP_
#define USERINTERFACE_HPP_
#include <SFGUI/SFGUI.hpp>
#include <SFGUI/Widgets.hpp>
#include "game/CardGame.hpp"

class Application;

class UserInterface
{
public:
	UserInterface(Application& application);
	~UserInterface() = default;

	void loadConfig();

	void minimizeUI();
	void maximizeUI();

	void handleEvent(const sf::Event& event, sf::Vector2f mouse_position);
	void update(sf::Time time);
	void draw(sf::RenderWindow& window);

private:
	Application& application;
	bool maximized;

	sfg::SFGUI m_sfgui;
	sfg::Desktop desktop;
	sfg::Window::Ptr m_window;
	sfg::Notebook::Ptr m_notebook;

		sfg::Box::Ptr m_box_main; //V
			sfg::Box::Ptr m_box_top; //H
				sfg::Frame::Ptr m_frame_new_game;
					sfg::Box::Ptr m_box_new_game; //V
						sfg::Button::Ptr m_button_solitaire;
						sfg::Button::Ptr m_button_spider;
				sfg::Frame::Ptr m_frame_current_game;
					sfg::Box::Ptr m_box_current_game; //V
						sfg::Button::Ptr m_button_resume;
						sfg::Button::Ptr m_button_restart;
						sfg::Button::Ptr m_button_save;
						sfg::Button::Ptr m_button_load;
			sfg::Box::Ptr m_box_bot; //H
				sfg::Frame::Ptr m_frame_options_general;
					sfg::Box::Ptr m_box_options_general; //V
						sfg::CheckButton::Ptr m_cbutton_show_fps;
						sfg::CheckButton::Ptr m_cbutton_vsync;
						sfg::CheckButton::Ptr m_cbutton_save;
						sfg::CheckButton::Ptr m_cbutton_load;
		//sfg::Frame::Ptr m_frame_options_solitaire;
		sfg::Box::Ptr m_box_main_settings;//V
			sfg::Frame::Ptr m_frame_options_solitaire;
				sfg::Box::Ptr m_box_options_solitaire; //V
					//sfg::RadioButton::Ptr m_rbutton_draw1;
					//sfg::RadioButton::Ptr m_rbutton_draw3;
					sfg::CheckButton::Ptr m_cbutton_show_score;
					sfg::CheckButton::Ptr m_cbutton_time_penalty;
			sfg::Frame::Ptr m_frame_options_spider;
				sfg::Box::Ptr m_box_options_spider; //V
					sfg::RadioButton::Ptr m_rbutton_colors1;
					sfg::RadioButton::Ptr m_rbutton_colors2;
					sfg::RadioButton::Ptr m_rbutton_colors4;
			sfg::Table::Ptr m_table_graphic_settings;
				sfg::Scale::Ptr m_scale_space;	sfg::Label::Ptr m_label_space;
				sfg::Scale::Ptr m_scale_slide;	sfg::Label::Ptr m_label_slide;

	sfg::Window::Ptr m_window_mini;
	sfg::Button::Ptr m_button_back;
	sf::RenderTexture m_render_texture;
	sf::Sprite m_sprite;
	float FPS;
	sf::Font FPS_font;
	sf::Text FPS_text;
	std::string FPS_to_draw;

	void clickedNewGame(CardGame::GameType game_type);
	void clickedRestart();
	void clickedSave();
	void clickedLoad();

	static std::string scaleToString(float value);
	void changedRenderSettings();
	void setActive(bool state);
};

#endif /* USERINTERFACE_HPP_ */
