#ifndef CONSTANT_H
#define CONSTANT_H

constexpr auto CARD_SIZE_X = 225;
constexpr auto CARD_SIZE_Y = 315;

#endif // CONSTANT_H
