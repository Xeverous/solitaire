#ifndef CARD_H
#define CARD_H
#include <SFML/Graphics.hpp>

/**
 * @file class representing a single card
 *
 * Card represents fundamental type for this program.
 * Since no cards can be created during any physical gameplay (no random card duplicates,
 * lost cards in the air etc) this class adheres to this concept by a rule of six
 *
 * The type Card is moveable but non-copyable
 */

class Card
{
public:
	/*
	 * @brief represents card's color
	 *
	 * @note red colors should be even and black odd
	 * @note order of the colors should match the order in sprite sheet
	 */ 
	enum class Color : unsigned char
	{
		heart = 0,             // ♥
		spade = 1, pike = 1,   // ♠
		tile = 2, diamond = 2, // ♦
		club = 3, clover = 3,  // ♣
		none // for joker-based games or game where it doesn't matter
	};

	struct Value
	{
		static constexpr unsigned char none = 0;
		static constexpr unsigned char  _1 =  1;
		static constexpr unsigned char  _2 =  2;
		static constexpr unsigned char  _3 =  3;
		static constexpr unsigned char  _4 =  4;
		static constexpr unsigned char  _5 =  5;
		static constexpr unsigned char  _6 =  6;
		static constexpr unsigned char  _7 =  7;
		static constexpr unsigned char  _8 =  8;
		static constexpr unsigned char  _9 =  9;
		static constexpr unsigned char _10 = 10;
		static constexpr unsigned char   J = 11;
		static constexpr unsigned char   Q = 12;
		static constexpr unsigned char   K = 13;
		static constexpr unsigned char AceHigh = 14;
		static constexpr unsigned char AceLow = _1;

		constexpr Value(unsigned value) : value(value) {}

		constexpr operator unsigned() { return value; }

		constexpr Value& operator++() { ++value; return *this; }
		constexpr Value operator++(int) { Value temp(*this); operator++(); return temp; }

	private:
		unsigned char value;
	};

	Card() = delete; // disallow creation of nonsense cards
	Card(Value value, Color color, bool visible = false);
	Card(bool visible); // joker's constructor

	// Cards can not be copied
	Card(const Card& other) = delete;
	Card& operator=(const Card& other) = delete;

	// Cards can only be moved
	Card(Card&& other) noexcept = default;
	Card& operator=(Card&& other) noexcept = default;

	~Card() = default;

	// events
	bool isMouseAbove(sf::Vector2f mouse_position) const;
	void handleEvent(const sf::Event& event, sf::Vector2f mouse_position);

	// game logic functions
	Value getValue() const;
	Color getColor() const;
	bool isRed() const;
	bool isBlack() const;
	bool isHighlighted() const;
	bool isVisible() const;
	void setVisibility(bool visible);

	// graphics functions
	void setHighlight(bool state);
	void setPosition(float x, float y);
	void setPosition(sf::Vector2f position);
	void setScale(float scale); ///< @brief Same as setScale(sf::Vector2f) but applies the same x and y
	void setScale(sf::Vector2f scale);
	const sf::Vector2f& GetPosition() const;

	void draw(sf::RenderTarget& window);

	static sf::Texture texture_back;
	static sf::Texture texture_joker;
	static sf::Texture texture_front;
	static sf::Texture texture_hover;

	static void loadAssets();

private:
	sf::Sprite sprite_front;
	sf::Sprite sprite_back;
	sf::Sprite sprite_highlight;
	bool highlight;
	bool visible;
	bool joker;
	Value value; // not const since joker can represent different values
	Color color; // as with value
};

#endif // CARD_H
