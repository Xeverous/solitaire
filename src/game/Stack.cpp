#ifndef STACK_CPP
#define STACK_CPP
#include "Stack.hpp"

#include <algorithm>
#include "../Constant.hpp"
#include "../Global.hpp"
#include "CardGame.hpp"

sf::Texture Stack::texture_base;

/* -------------------------------------------------------------------
 * 				PUBLIC FUNCTIONS
 * Creation / Rendering
 */
Stack::Stack(Slide slide, sf::Vector2f pos)
	: first_highlighted_card(stack.end()), slide(slide), mouse_above_base(false)
{
	sprite.setTexture(texture_base);
	sprite.setTextureRect(sf::IntRect(0, 0, CARD_SIZE_X, CARD_SIZE_Y));
	sprite.setPosition(pos);
}

void Stack::loadAssets()
{
	texture_base = global::resource_holder.acquire(global::Asset::stack_empty, thor::Resources::fromFile<sf::Texture>(global::getAssetPath(global::Asset::stack_empty)));
}

void Stack::SetSliding(Slide new_slide)
{
	slide = new_slide;
	refreshCardsPositions();
}

void Stack::refreshCardsPositions()
{
	auto& pos = sprite.getPosition();

	if (slide == Slide::vertical)
		for (unsigned i = 0; i < stack.size(); i++)
			stack[i].setPosition(pos.x, pos.y + i * render_settings::card_slide.y);

	else if (slide == Slide::horizontal)
		for (unsigned i = 0; i < stack.size(); i++)
			stack[i].setPosition(pos.x + i * render_settings::card_slide.x, pos.y);

	else if (slide == Slide::none)
		for (auto& card : stack)
			card.setPosition(pos.x, pos.y);
}

void Stack::refreshCardsScale()
{
	for (auto& card : stack)
		card.setScale(sprite.getScale());
}

void Stack::setPosition(sf::Vector2f position)
{
	sprite.setPosition(position);
	refreshCardsPositions();
}

void Stack::setPositionGrab(sf::Vector2f mouse_position)
{
	float y_offset;

	if (stack.size() > 1)
		y_offset = render_settings::card_slide.y;
	else
		y_offset = sprite.getGlobalBounds().height;

	sprite.setPosition(
		mouse_position.x - sprite.getGlobalBounds().width / 2.f,
		mouse_position.y - y_offset                       / 2.f);

	refreshCardsPositions();
}

void Stack::setScale(float scale)
{
	sprite.setScale(scale, scale);
	for (auto& card : stack)
		card.setScale(scale);
}

void Stack::update(sf::Time time)
{
	// no animations so far
	(void)time;
}

void Stack::draw(sf::RenderWindow& window)
{
	if (stack.empty())
	{
		window.draw(sprite);
	}
	else
	{
		if (slide == Slide::none)
		{
			// only last card is visible
			stack.back().draw(window);
		}
		else
		{
			for (auto& card : stack)
				card.draw(window);
		}
	}
}

void Stack::handleHighlight(sf::Vector2f mouse_position)
{
	mouse_above_base = sprite.getGlobalBounds().contains(mouse_position);

	first_highlighted_card = stack.end();
	for (auto it = stack.begin(); it != stack.end(); ++it)
		if (it->isMouseAbove(mouse_position))
			first_highlighted_card = it;

	for (auto it = stack.begin(); it != first_highlighted_card; ++it)
		it->setHighlight(false);

	for (auto it = first_highlighted_card; it != stack.end(); ++it)
		it->setHighlight(true);
}
/* -------------------------------------------------------------------
 * 				CONTAINER MANIPULATION
 */
std::size_t Stack::size() const
{
	return stack.size();
}

bool Stack::empty() const
{
	return stack.empty();
}

void Stack::clear()
{
	stack.clear();
}

void Stack::push_front(Card&& card)
{
	stack.push_front(std::move(card));

	/*
	 * no card position adjust
	 * since it's mean to be only used for Hand stack
	 * which is always set and refreshed by mouse position
	 */
}

void Stack::pop_front()
{
	stack.pop_front();
}

void Stack::push_back(Card&& card)
{
	stack.push_back(std::move(card));

	auto& pos = sprite.getPosition();

	if (slide == Slide::vertical)
		stack.back().setPosition(pos.x, pos.y + (stack.size() - 1) * render_settings::card_slide.y);
	else if (slide == Slide::horizontal)
		stack.back().setPosition(pos.x + (stack.size() - 1) * render_settings::card_slide.x, pos.y);
	else if (slide == Slide::none)
		stack.back().setPosition(pos.x, pos.y);
}

void Stack::pop_back()
{
	stack.pop_back();
}

Card& Stack::back()
{
	return stack.back();
}

const Card& Stack::back() const
{
	return stack.back();
}

Card& Stack::front()
{
	return stack.front();
}

const Card& Stack::front() const
{
	return stack.front();
}

Card& Stack::operator[](std::size_t pos)
{
	return stack[pos];
}

const Card& Stack::operator[](std::size_t pos) const
{
	return stack[pos];
}

void Stack::moveCardTo(Stack& target)
{
	target.push_back(std::move(stack.back()));
	stack.pop_back();
}

void Stack::moveAllCardsTo(Stack& target)
{
	while (!stack.empty())
		moveCardTo(target);
}

void Stack::pickCardFrom(Stack& source)
{
	stack.push_front(std::move(source.back()));
	source.pop_back();
}

void Stack::pickAllCardsFrom(Stack& source)
{
	while (!source.empty())
		pickCardFrom(source);
}

void Stack::dropCardTo(Stack& target)
{
	target.push_back(std::move(stack.front()));
	stack.pop_front();
}

void Stack::dropAllCardsTo(Stack& target)
{
	while (!stack.empty())
		dropCardTo(target);
}

/* -------------------------------------------------------------------
 * 				CARD MANIPULATION
 * set/get properties from/to cards
 */

void Stack::newDeckStandard52(unsigned jokers)
{
	for (unsigned i = 0; i < 4; ++i)
		for (Card::Value v = Card::Value::AceLow; v <= Card::Value::K; ++v)
			stack.emplace_back(v, static_cast<Card::Color>(i));

	newJokers(jokers);
	refreshCardsPositions();
	refreshCardsScale();
}

void Stack::newDeckStandard24(unsigned jokers)
{
	for (unsigned i = 0; i < 4; ++i)
		for (Card::Value v = Card::Value::_9; v <= Card::Value::AceHigh; ++v)
			stack.emplace_back(v, static_cast<Card::Color>(i));

	newJokers(jokers);
	refreshCardsPositions();
	refreshCardsScale();
}

void Stack::newJokers(unsigned jokers)
{
	for (unsigned i = 0; i < jokers; i++)
		stack.emplace_back(false);
}

void Stack::newDeckColorSet13(Card::Color color, unsigned copies)
{
	for (unsigned i = 0; i < copies; i++)
		for (Card::Value v = Card::Value::AceLow; v <= Card::Value::K; ++v)
			stack.emplace_back(v, color);

	refreshCardsPositions();
	refreshCardsScale();
}

void Stack::shuffle()
{
	std::shuffle(stack.begin(), stack.end(), global::mt);
}

void Stack::showAllCards()
{
	for (auto& card : stack)
		card.setVisibility(true);
}

void Stack::hideAllCards()
{
	for (auto& card : stack)
		card.setVisibility(false);
}

void Stack::setTopCardVisibility(bool vis)
{
	stack.back().setVisibility(vis);
}

void Stack::setCardVisibility(std::size_t pos, bool vis)
{
	stack[pos].setVisibility(vis);
}

bool Stack::getTopCardVisibility() const
{
	return stack.back().isVisible();
}

void Stack::setAllCardsHighlight(bool state)
{
	for (auto& card : stack)
		card.setHighlight(state);
}

/* -------------------------------------------------------------------
 * 				LOGIC FUNCTIONS
 */

bool Stack::isMouseAboveBase() const
{
	return mouse_above_base;
}

bool Stack::isAnyCardHighlighted() const
{
	return first_highlighted_card != stack.end();
}

bool Stack::isCovered() const
{
	return isMouseAboveBase() or isAnyCardHighlighted();
}

bool Stack::areAllHighlightedCardsVisible() const
{
	return std::all_of(
		static_cast<decltype(stack)::const_iterator>(first_highlighted_card),
		stack.cend(),
		[](const Card& card){ return card.isVisible(); });
}

unsigned Stack::getHighlightedCardsAmount() const
{
	return stack.end() - first_highlighted_card;
}

unsigned Stack::getFirstHighlightedCardIndex() const
{
	return first_highlighted_card - stack.begin();
}

#endif // STACK_CPP
