#ifndef GAME_H_
#define GAME_H_
#include <Thor/Input.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>

class Game
{
private:
	void mousePressLeft();
	void mousePressRight();
	void mouseMove();
	void mouseReleaseLeft();
	void mouseReleaseRight();

	bool mouse_moving_left;
	bool mouse_moving_right;

protected:
	thor::ActionMap<std::string> action_map;
	thor::ActionMap<std::string>::CallbackSystem system;

	virtual void mouseClickLeft() = 0;
	virtual void mouseClickRight() = 0;
	virtual void mouseHoldStartLeft() = 0;
	virtual void mouseHoldStartRight() = 0;
	virtual void mouseHoldEndLeft() = 0;
	virtual void mouseHoldEndRight() = 0;

	void handleEventByActionMap(const sf::Event& event, thor::ActionMap<std::string>::CallbackSystem& system, sf::RenderWindow* window);

public:
	Game();
	virtual ~Game() = default;
};

#endif /* GAME_H_ */
