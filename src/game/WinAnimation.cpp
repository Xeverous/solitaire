#include "WinAnimation.hpp"

#include <stdexcept>
#include <random>
#include "../Global.hpp"

void WinAnimation::update(sf::Time time)
{
	if (cards.empty())
		return;

	Card& card = cards.back();

	speed.y += acceleration * time.asSeconds();
	card.setPosition(card.GetPosition() + speed * time.asSeconds());

	// check if card has reached the bottom
	if (card.GetPosition().y + render_settings::card_scaled_size.y > static_cast<float>(global::window_size.y))
	{
		sf::Vector2f pos = card.GetPosition();
		card.setPosition(
			pos.x,
			2.0f * (static_cast<float>(global::window_size.y) - render_settings::card_scaled_size.y) - pos.y);

		// invert speed (bounce) and lose some energy
		speed.y = -speed.y;
		speed.y = speed.y * bounce_factor;
	}

	// check if card has reached left/right side
	if (card.GetPosition().x + render_settings::card_scaled_size.x < 0.0f or card.GetPosition().x > global::window_size.x)
	{
		// it is time to drop it and pick up another card
		cards.pop_back();
		resetVariables();
	}

	time_passed += time;
}

void WinAnimation::draw(sf::RenderWindow& window)
{
	if (time_passed > draw_period)
	{
		time_passed %= draw_period;

		if (!cards.empty())
			cards.back().draw(render_texture);
	}

	render_texture.display();
	sprite.setTexture(render_texture.getTexture());
	window.draw(sprite);

	if (!cards.empty())
		cards.back().draw(window);
}

void WinAnimation::resetVariables()
{
	std::uniform_real_distribution<float> dist_x(15.0f, 50.0f);
	float x = dist_x(global::mt);

	std::uniform_real_distribution<float> dist_y(-90.0f, 0.0f);
	float y = dist_y(global::mt);

	std::bernoulli_distribution sign;
	if (sign(global::mt))
		x = -x;

	speed = sf::Vector2f(x, y);
}

void WinAnimation::clear()
{
	cards.clear();
}

void WinAnimation::start()
{
	if (!render_texture.create(global::window_size.x, global::window_size.y))
		throw std::runtime_error("Failed to create render texture\n");

	for (auto& card : cards)
		card.setHighlight(false);

	for (auto& card : cards)
		card.setVisibility(true);

	for (auto& card : cards)
		card.draw(render_texture);

	resetVariables();
}
