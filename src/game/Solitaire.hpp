#ifndef SOLITAIRE_H_
#define SOLITAIRE_H_
#include <array>
#include "CardGame.hpp"
#include "Stack.hpp"

class Solitaire : public CardGame
{
public:
	Solitaire();
	virtual ~Solitaire();
	void newGame() override;
	void restart() override;
	void save() override;
	void load() override;
	void reinitRender() override;

	void handleEvent(const sf::Event& event, sf::Vector2f mouse_position) override;
	void update(sf::Time time) override;
	void draw(sf::RenderWindow& window) override;

private:
	Stack stock;
	Stack waste;
	std::array<Stack, 4> foundations;
	std::array<Stack, 7> tableaux;
	Stack hand;
	Stack* last_pickup = nullptr; ///< stack where to rollback on invalid move

	void mouseClickLeft() override;
	void mouseClickRight() override;
	void mouseHoldStartLeft() override;
	void mouseHoldEndLeft() override;
	void mouseHoldEndRight() override;

	/**
	 * @brief Tries to pick up all highlighted cards from the stack to hand
	 * @details checks that the stack is not empty
	 * @note hand must be empty
	 * @return true if pick up was successful
	 */
	bool tryToPickUpAllHighlightedCards(Stack& stack);
	/**
	 * @brief Move the top card to hand
	 * @details For a successful move:
	 *  - stack must be hovered
	 *  - stack must contain >= 1 card
	 *  - top card must be visible
	 * @return true if pick up was successful
	 */
	bool tryToPickUpTopCard(Stack& stack);
	/**
	 * @brief Attempt to drop cards to a given tableaux with respect to Solitaire rules
	 * @details If stack is empty, Hand's first card must be a King
	 * If stack is not empty, Hand's first card must have opposite color and value lower by 1
	 * @note hand can not be empty
	 * @return true if drop was successful
	 */
	bool tryToDropOnTableaux(Stack& stack);
	/**
	 * @brief Attempt to drop cards to a given foundation with respect to Solitaire rules
	 * @param origin stack from which card should be moved, can not be empty
	 * @param foundation foundation on which to put the card, can be empty
	 * @return true if drop was successful
	 */
	bool tryToDropOnFoundation(Stack& origin, Stack& foundation);
	/**
	 * @brief Same as tryToDropOnFoundation() but tries on all foundations
	 * @param origin stack from which card should be moved, can not be empty
	 * @return true if drop was successful
	 */
	bool tryToDropOnAllFoundations(Stack& origin);

	bool checkWin() const;
	void rollback();
};

#endif /* SOLITAIRE_H_ */
