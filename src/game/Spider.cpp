#include "Spider.hpp"

#include "../Constant.hpp"
#include "../Global.hpp"

Spider::Spider(unsigned colors)
	: last_pickup(nullptr), stocks_opened(0), groups_completed(0), colors(colors)
{
	hand.SetSliding(Stack::Slide::vertical);

	for (auto& stack : tableaux)
		stack.SetSliding(Stack::Slide::vertical);

	newGame();
}

Spider::~Spider()
{
	save();
}

void Spider::newGame()
{
	groups_completed = 0;
	stocks_opened = 0;

	hand.clear();
	for (auto& stack : foundations)
		stack.clear();

	for (auto& stack : tableaux)
		stack.clear();

	for (auto& stack : stocks)
		stack.clear();

	for (unsigned i = 0; i < colors; i++)
		hand.newDeckColorSet13(static_cast<Card::Color>(3 - i), 8 / colors);

	hand.shuffle();

	// 54 (10x5 + 4) cards to tableaux
	for (auto& stack : tableaux)
		for (unsigned i = 0; i < 5; i++)
			hand.moveCardTo(stack);

	for (unsigned i = 0; i < 4; i++)
		hand.moveCardTo(tableaux[i]);

	for (auto& stack : tableaux)
		stack.setTopCardVisibility(true);

	// 50 (5x10) cards to stocks
	for (auto& stack : stocks)
		for (unsigned i = 0; i < tableaux.size(); i++)
			hand.moveCardTo(stack);

	reinitRender();
}

void Spider::restart()
{

}

void Spider::save()
{

}

void Spider::load()
{

}

void Spider::reinitRender()
{
	recalculateScale(sf::Vector2u(tableaux.size(), 2), sf::Vector2i(0, 20));

	for (unsigned i = 0; i < foundations.size(); i++)
		foundations[i].setPosition(generateStackPosition({0, 1}, {static_cast<int>(i), 20}));

	for (unsigned i = 0; i < tableaux.size(); i++)
		tableaux[i].setPosition(generateStackPosition({i, 0}));

	for (unsigned i = 0; i < stocks.size(); i++)
		stocks[i].setPosition(generateStackPosition({tableaux.size() - 1, 1}, {static_cast<int>(i) - 4, 20}));

	using render_settings::scale;

	hand.setScale(scale);

	for (auto& stack : stocks)
		stack.setScale(scale);

	for (auto& stack : foundations)
		stack.setScale(scale);

	for (auto& stack : tableaux)
		stack.setScale(scale);
}

void Spider::handleEvent(const sf::Event& event, sf::Vector2f mouse_position)
{
	if (!active)
		return;

	if (!hand.empty())
		hand.setPositionGrab(mouse_position);

	for (auto& stack : tableaux)
		stack.handleHighlight(mouse_position);

	if (stocks_opened < stocks.size())
		stocks[stocks_opened].handleHighlight(mouse_position);

	handleEventByActionMap(event, system, nullptr);
}

void Spider::update(sf::Time time)
{
	if (!active)
		return;

	hand.update(time);

	for (auto& stack : tableaux)
		stack.update(time);

	for (auto& stack : stocks)
		stack.update(time);

	if (won)
		win_animation.update(time);
}

void Spider::draw(sf::RenderWindow& window)
{
	if (!active)
		return;

	for (auto& stack : foundations)
		if (stack.empty()) // do not draw empty foundations
			break;
		else
			stack.draw(window);

	for (auto& stack : tableaux)
		stack.draw(window);

	for (unsigned i = stocks.size(); i > stocks_opened; i--) // reversed for left-fold Z-order
		stocks[i - 1].draw(window);

	if (!hand.empty()) // don't draw hand's rectangle
		hand.draw(window);

	if (won)
		win_animation.draw(window);
}

//-------------------------------------------------------------------
// MOUSE CLICKS

void Spider::mouseClickLeft()
{
	if (stocks_opened < stocks.size())
		if (stocks[stocks_opened].isAnyCardHighlighted())
		{
			for (auto& stack : tableaux)
				if (stack.empty())
					return; // game rule: can not open stock if any tableaux is empty

			for (auto& stack : tableaux)
				stocks[stocks_opened].moveCardTo(stack);

			for (auto& stack : tableaux)
				stack.setTopCardVisibility(true);

			stocks_opened++;

			for (auto& stack : tableaux)
				searchForCompleteSet(stack); // a full set might be created after opening a stock

			return;
		}

	// search for cards to uncover
	for (auto& stack : tableaux)
		if (stack.isAnyCardHighlighted())
		{
			stack.setTopCardVisibility(true);
			return;
		}
}

void Spider::mouseHoldStartLeft()
{
	if (!hand.empty())
		return;

	for (auto& stack : tableaux)
		if (stack.isCovered())
		{
			if (tryToPickUpCards(stack))
				last_pickup = &stack;

			return;
		}
}

void Spider::mouseHoldEndLeft()
{
	if (hand.empty()) // nothing to drop
	{
		mouseClickLeft(); // QoL to uncover cards
		return;
	}

	for (auto& stack : tableaux)
		if (stack.isCovered())
		{
			if (tryToDropCards(stack))
				return;

			break;
		}

	rollback(); // nothing was found to drop to, drop cards from hand to where they were
}

/* -------------------------------------------------------------------
 * 			TRYING FUNCTIONS
 * all of these returns false if the operation fails
 */
bool Spider::tryToPickUpCards(Stack& stack)
{
	if (stack.empty())
		return false;

	if (!stack.areAllHighlightedCardsVisible())
		return false;

	unsigned first = stack.getFirstHighlightedCardIndex();
	Card::Value first_value = stack[first].getValue();
	Card::Color first_color = stack[first].getColor();

	for (unsigned i = first + 1; i < stack.size(); i++)
	{
		if (stack[i].getValue() + (i - first) != first_value)
			return false;

		if (stack[i].getColor() != first_color)
			return false;
	}

// another way to do it, new one seems more clear
//	const unsigned stack_size = stack.size();
//	for (unsigned i = first; i < stack_size; i++)
//		hand.pickCardFrom(stack);

	unsigned picked = stack.getHighlightedCardsAmount();
	for (unsigned i = 0; i < picked; i++)
		hand.pickCardFrom(stack);

	return true;
}

bool Spider::tryToDropCards(Stack& stack)
{
	if (stack.empty())
	{
		hand.dropAllCardsTo(stack);
		return true;
	}

	if (!stack.getTopCardVisibility())
		return false;

	if (stack.back().getValue() != hand.front().getValue() + 1)
		return false;

	if (stack.back().getColor() != hand.front().getColor())
		return false;

	hand.dropAllCardsTo(stack);
	searchForCompleteSet(stack);
	return true;
}

void Spider::searchForCompleteSet(Stack& stack)
{
	const unsigned complete_set_size = 1 + Card::Value::K - Card::Value::AceLow;

	for (unsigned king_pos = 0; king_pos < stack.size(); king_pos++)
	{
		// find a visible King first
		if (!stack[king_pos].isVisible())
			continue;

		if (stack[king_pos].getValue() != Card::Value::K)
			continue;

		// check if stack has enough cards
		if (stack.size() < king_pos + complete_set_size)
			return;

		auto verify_set = [&stack, king_pos]() -> bool
		{
			for (unsigned j = king_pos + 1; j < king_pos + complete_set_size; j++)
			{
				if (stack[j].getValue() + j - king_pos != Card::Value::K)
					return false;

				if (stack[j].getColor() != stack[king_pos].getColor())
					return false;
			}

			return true;
		};

		if (!verify_set())
			continue;

		// valid set found - move the cards
		for (Card::Value i = Card::Value::AceLow; i <= Card::Value::K; ++i)
			stack.moveCardTo(foundations[groups_completed]);

		foundations[groups_completed].setAllCardsHighlight(false);

		checkWin();
		return;
	}
}

void Spider::checkWin()
{
	// use tuple size because .size() is not static
	constexpr unsigned max_sets = std::tuple_size<decltype(foundations)>::value;

	if (++groups_completed == max_sets)
		won = true;
	else
		return;

	// size() is not static
	static_assert(
		std::tuple_size<decltype(tableaux   )>::value >=
		std::tuple_size<decltype(foundations)>::value,
		"Not enough tableaux stacks for win animation.");

	for (unsigned i = 0; i < max_sets; ++i)
		foundations[i].moveAllCardsTo(tableaux[i]);

	win_animation.clear();
	win_animation.addCardsFromStacks(tableaux.begin(), tableaux.begin() + max_sets);
	win_animation.start();
}

void Spider::rollback()
{
	hand.dropAllCardsTo(*last_pickup);
}
