#ifndef STACK_H
#define STACK_H
#include <SFML/Graphics.hpp>
#include <SFGUI/Widgets.hpp>
#include <deque>
#include "Card.hpp"

class Stack
{
public:
	enum class Slide
	{
		none,
		horizontal,
		vertical
	};

	/*
	 * Stack represents one of the fundamental types in any card game
	 * As with class Card, it adheres to the no-copy rule - there is no logical point in copying cards
	 */
	Stack(Slide slide = Slide::none, sf::Vector2f pos = sf::Vector2f());

	Stack(const Stack& other) = delete;
	Stack& operator=(const Stack& other) = delete;

	Stack(Stack&& other) noexcept = default;
	Stack& operator=(Stack&& other) noexcept = default;

	~Stack() = default;

	void SetSliding(Slide slide);

	// POSITIONING

	/**
	 * @brief Standard position setting, as the cards lie on the table
	 * @param position top-left corner
	 */
	void setPosition(sf::Vector2f position);
	/**
	 * @brief Set position as the hand would hold cards
	 * @details If there is more than 1 card in the stack, cards will be hold with a slide.
	 * Otherwise, for a single card, it will be hold by the center
	 */
	void setPositionGrab(sf::Vector2f mouse_position);
	/*
	 * SCALING
	 */
	void setScale(float scale);
	/*
	 * EVENT HANDLING
	 */
	void update(sf::Time time);
	void draw(sf::RenderWindow& window);
	void handleHighlight(sf::Vector2f mouse_position);

	/*
	 * CONTAINER MANIPULATION
	 * typical functions, follows everything
	 * into internal std::deque<Card>
	 */
	std::size_t size() const;
	bool empty() const;
	void clear();

	void push_front(Card&& card);
	void push_front(const Card& card) = delete;
	void pop_front();
	void push_back(Card&& card);
	void push_back(const Card& card) = delete;
	void pop_back();

	Card& front();
	Card& back();
	Card& operator[](std::size_t pos);
	const Card& front() const;
	const Card& back() const;
	const Card& operator[](std::size_t pos) const;

	/**
	 * @brief Move 1 card from the top to another stack
	 * @details Intended for moving cards between regular stacks.
	 * Cards placed on the target stack will have reverse order.
	 */
	void moveCardTo(Stack& target);
	/**
	 * @brief Move all cards to another stack
	 * @details Same as moveCardTo(), but moves all cards instead
	 */
	void moveAllCardsTo(Stack& target);
	/**
	 * @brief Pick 1 card from the top of another stack
	 * @details Intended for moving cards from table to hand (pick up effect).
	 * Picked cards will have the same order
	 */
	void pickCardFrom(Stack& source);
	/**
	 * @brief Pick all cards from another stack
	 * @details Same as pickCardFrom(), but picks all cards instead
	 */
	void pickAllCardsFrom(Stack& source);
	/**
	 * @brief Drop 1 card to the top of another stack
	 * @details Intended for moving cards from hand to table (drop effect)
	 * Dropped cards will have the same order
	 */
	void dropCardTo(Stack& target);
	/**
	 * @brief Drop all cards to another stack
	 * @details Same as dropCardTo(), but drops all cards instead
	 */
	void dropAllCardsTo(Stack& target);

	/*
	 * CARD MANIPULATION
	 */
	void newDeckStandard52(unsigned jokers = 0); // 4 colors, each (A, 2, ..., 10, J, Q, K) + possible jokers (should be in range 0 ... 3)
	void newDeckStandard24(unsigned jokers = 0); // 4 colors, each         (9, 10, J, Q, K) + possible jokers (should be in range 0 ... 3)
	void newDeckColorSet13(Card::Color color, unsigned copies = 1); // _copies_ sets of (A, 2, ..., 10, J, Q, K) in specified color
	void shuffle();

	void showAllCards();
	void hideAllCards();
	bool getTopCardVisibility() const;
	/**
	 * @brief Set whether first card should be visible or not
	 * @note This function can only be called when stack has at least 1 card
	 */
	void setTopCardVisibility(bool visible);
	void setCardVisibility(std::size_t pos, bool visible);
	void setAllCardsHighlight(bool state);

	// LOGIC FUNCTIONS

	/**
	 * @brief Check whether mouse is located above the stack's base area
	 * @return true if so
	 */
	bool isMouseAboveBase() const; // for non-sliding stacks, same as isHighlighted but returns true even if its empty
	/**
	 * @brief Check if any card has been highlighted
	 * @details If the stack is empty, this function always returns false
	 * If the stack has slide set to none, and this function returns true this implies true return of isMouseAboveBase().
	 * If the stack has slide set to none, and is not empty, return value is the same as of isMouseAboveBase().
	 * @return true if so
	 */
	bool isAnyCardHighlighted() const;
	/**
	 * @brief Check whether stack has either base or cards covered by mouse
	 * @return isMouseAboveBase() OR isAnyCardHighlighted();
	 */
	bool isCovered() const;
	/**
	 * @brief Check if all highlighted cards are visible
	 * @details If the stack is empty this function always return true
	 * @return true if all highlighted cards are visible
	 */
	bool areAllHighlightedCardsVisible() const;
	/**
	 * @brief Get how many cards are highlighted
	 * @return a value in range [0, stack.size()]
	 */
	unsigned getHighlightedCardsAmount() const;
	/**
	 * @brief Get index of first highlighted card
	 * @warning stack must have a highlighted card
	 * @return index
	 */
	unsigned getFirstHighlightedCardIndex() const;

	static sf::Texture texture_base;
	static void loadAssets();

private:
	sf::Sprite sprite;
	std::deque<Card> stack;
	decltype(stack)::iterator first_highlighted_card;
	Slide slide;
	bool mouse_above_base;

	void newJokers(unsigned jokers);

	void refreshCardsPositions(); // by the position of sprite
	void refreshCardsScale(); // by the scale of sprite
};

#endif // STACK_H
