#ifndef GAME_WINANIMATION_HPP_
#define GAME_WINANIMATION_HPP_
#include <SFML/Graphics.hpp>
#include <vector>
#include <array>
#include <iterator>
#include <type_traits>
#include "Stack.hpp"

class WinAnimation
{
public:
	void update(sf::Time time);
	void draw(sf::RenderWindow& window);

	/**
	 * @brief Creates render texture and starts animation
	 * @details all cards will be rendered on first frame
	 * @note may throw std::runtime_error if render texture can not be created
	 */
	void start();
	void clear(); ///< stops the animation and removes all cards

	template <typename ForwardIt>
	void addCardsFromStacks(ForwardIt first, ForwardIt last);

private:
	/**
	 * @brief call when a new card is animated
	 * @details rolls a new speed and direction for the card
	 */
	void resetVariables();

	sf::RenderTexture render_texture;
	sf::Sprite sprite;
	/**
	 * @brief how much speed is left after bounce
	 * @details (should be in range 0.0 ... 1.0)
	 * 0.0 means complete loss of energy, 1.0 means no loss
	 */
	const float bounce_factor = 0.75f;
	const float acceleration = 9.80665f * 5.0f; ///< @brief gravity, in pixels/s^2
	const sf::Time draw_period = sf::milliseconds(75); ///< @brief time period after which card is drawed again
	sf::Time time_passed = sf::Time::Zero; ///< @brief time passed since last card drawing; this should be always less than draw_period
	sf::Vector2f speed; ///< @brief speed at which card moves (pixels/s), y part should increase in time due to accelleration
	std::vector<Card> cards;
};

template <typename ForwardIt>
void WinAnimation::addCardsFromStacks(ForwardIt first, ForwardIt last)
{
	static_assert(std::is_same
	<
		typename std::remove_reference<typename std::iterator_traits<ForwardIt>::value_type>::type,
		Stack
	>::value, "Iterator must be for type Stack");

	bool moved_a_card;

	do {
		moved_a_card = false;

		while (first++ != last)
		{
			if (!first->empty())
			{
				cards.push_back(std::move(first->front()));
				first->pop_front();
				moved_a_card = true;
			}
		}
	} while (moved_a_card);
}

#endif /* GAME_WINANIMATION_HPP_ */
