#ifndef CARD_CPP
#define CARD_CPP
#include "Card.hpp"

#include <SFML/Graphics.hpp>
#include "../Constant.hpp"
#include "../Global.hpp"

sf::Texture Card::texture_back;
sf::Texture Card::texture_joker;
sf::Texture Card::texture_front;
sf::Texture Card::texture_hover;

Card::Card(Value value, Color color, bool visible)
	: highlight(false),
	  visible(visible),
	  joker(false),
	  value(value),
	  color(color)
{
	if (joker)
	{
		sprite_front.setTexture(texture_joker);
		sprite_front.setTextureRect(sf::IntRect(0, 0, CARD_SIZE_X, CARD_SIZE_Y));
	}
	else
	{
		sprite_front.setTexture(texture_front);
		sprite_front.setTextureRect(sf::IntRect((value-1) * CARD_SIZE_X, (static_cast<int>(color)) * CARD_SIZE_Y, CARD_SIZE_X, CARD_SIZE_Y));
	}

	sprite_back.setTexture(Card::texture_back);
	sprite_back.setTextureRect(sf::IntRect(0, 0, CARD_SIZE_X, CARD_SIZE_Y));

	sprite_highlight.setTexture(texture_hover);
}

void Card::loadAssets()
{
	texture_front = global::resource_holder.acquire(global::Asset::card_front, thor::Resources::fromFile<sf::Texture>(global::getAssetPath(global::Asset::card_front)));
	texture_back  = global::resource_holder.acquire(global::Asset::card_back,  thor::Resources::fromFile<sf::Texture>(global::getAssetPath(global::Asset::card_back)));
	texture_joker = global::resource_holder.acquire(global::Asset::card_joker, thor::Resources::fromFile<sf::Texture>(global::getAssetPath(global::Asset::card_joker)));
	texture_hover = global::resource_holder.acquire(global::Asset::card_hover, thor::Resources::fromFile<sf::Texture>(global::getAssetPath(global::Asset::card_hover)));
}

void Card::handleEvent(const sf::Event& event, sf::Vector2f mouse_position)
{
	highlight = (event.type == sf::Event::MouseMoved and isMouseAbove(mouse_position));
}

Card::Card(bool visible): highlight(false), visible(visible), joker(true), value(Value::none), color(Color::none)
{
	
}

void Card::setVisibility(bool vis)
{
	visible = vis;
}

bool Card::isVisible() const
{
	return visible;
}

bool Card::isHighlighted() const
{
	return highlight;
}

void Card::setHighlight(bool state)
{
	highlight = state;
}

void Card::setPosition(sf::Vector2f pos)
{
	setPosition(pos.x, pos.y);
}

void Card::setPosition(float x, float y)
{
	sprite_front.setPosition(x, y);
	sprite_back.setPosition(x, y);
	sprite_highlight.setPosition(x, y);
}

const sf::Vector2f& Card::GetPosition() const
{
	return sprite_front.getPosition();
}

void Card::setScale(float scale)
{
	setScale({scale, scale});
}

void Card::setScale(sf::Vector2f scale)
{
	sprite_front.setScale(scale);
	sprite_back.setScale(scale);
	sprite_highlight.setScale(scale);
}

bool Card::isMouseAbove(sf::Vector2f mouse_position) const
{	
	return sprite_front.getGlobalBounds().contains(mouse_position);
}

Card::Value Card::getValue() const
{
	return value;
}

Card::Color Card::getColor() const
{
	return color;
}

bool Card::isRed() const
{
	return static_cast<int>(color) % 2 == 0;
}

bool Card::isBlack() const
{
	return !isRed();
}

void Card::draw(sf::RenderTarget& window)
{
	if (visible)
		window.draw(sprite_front);
	else
		window.draw(sprite_back);

	if (highlight)
		window.draw(sprite_highlight);
}

#endif // CARD_CPP
