#include "CardGame.hpp"

#include <SFML/Graphics.hpp>
#include "../Constant.hpp"
#include "../Global.hpp"

float CardGame::CARD_SPACE_FACTOR;
float CardGame::CARD_SLIDE_FACTOR;

CardGame::CardGame() : won(false), active(false)
{

}

sf::Vector2f CardGame::generateStackPosition(sf::Vector2u stacks, sf::Vector2i slides)
{
	namespace rs = render_settings;

	return {
		rs::card_space.x + static_cast<float>(stacks.x) * (rs::card_scaled_size.x + rs::card_space.x) + static_cast<float>(slides.x) * rs::card_slide.x,
		rs::card_space.y + static_cast<float>(stacks.y) * (rs::card_scaled_size.y + rs::card_space.y) + static_cast<float>(slides.y) * rs::card_slide.y};
}

void CardGame::recalculateScale(sf::Vector2u stacks, sf::Vector2i slides)
{
	sf::Vector2f requirement; // in pixels
	requirement.x = (((static_cast<float>(stacks.x) + 1.0f) * CARD_SPACE_FACTOR) + stacks.x + (static_cast<float>(slides.x) * CARD_SLIDE_FACTOR)) * CARD_SIZE_X;
	requirement.y = (((static_cast<float>(stacks.y) + 1.0f) * CARD_SPACE_FACTOR) + stacks.y + (static_cast<float>(slides.y) * CARD_SLIDE_FACTOR)) * CARD_SIZE_Y;

	namespace rs = render_settings;
	rs::scale = std::min(global::window_size.x / requirement.x, global::window_size.y / requirement.y);

	rs::card_scaled_size = sf::Vector2f(CARD_SIZE_X, CARD_SIZE_Y) * rs::scale;
	rs::card_space = rs::card_scaled_size * CARD_SPACE_FACTOR;
	rs::card_slide = rs::card_scaled_size * CARD_SLIDE_FACTOR;
}

void CardGame::setActive(bool status)
{
	if (status)
		resume();
	else
		pause();
}

void CardGame::pause()
{
	active = false;
}

void CardGame::resume()
{
	active = true;
	reinitRender();
}

void CardGame::mouseClickLeft()
{
}

void CardGame::mouseClickRight()
{
}

void CardGame::mouseHoldStartLeft()
{
}

void CardGame::mouseHoldStartRight()
{
}

void CardGame::mouseHoldEndLeft()
{
}

void CardGame::mouseHoldEndRight()
{
}
