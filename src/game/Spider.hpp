#ifndef SPIDER_H_
#define SPIDER_H_
#include "CardGame.hpp"
#include "Stack.hpp"

class Spider : public CardGame
{
public:
	Spider() = delete;
	Spider(unsigned colors);
	virtual ~Spider();
	void newGame() override;
	void restart() override;
	void save() override;
	void load() override;
	void reinitRender() override;

	void handleEvent(const sf::Event& event, sf::Vector2f mouse_position) override;
	void update(sf::Time time) override;
	void draw(sf::RenderWindow& window) override;

private:
	std::array<Stack, 5> stocks;
	std::array<Stack, 8> foundations;
	std::array<Stack, 10> tableaux;
	Stack hand;
	Stack* last_pickup; ///< stack on which to drop cards from hand on invalid move

	unsigned stocks_opened;
	unsigned groups_completed;
	const unsigned colors;

	void mouseClickLeft() override;
	void mouseHoldStartLeft() override;
	void mouseHoldEndLeft() override;

	/**
	 * @brief Pick up highlighted cards if possible according to Spider rules
	 * @param stack stack on which to attempt pick up
	 * @note Hand should be empty
	 * @return true if pick up was successful
	 */
	bool tryToPickUpCards(Stack& stack);
	/**
	 * @brief Drop cards in hand to the stack with respect to Spider rules
	 * @param stack stack on which attempt to drop
	 * @note Hand should not be empty
	 * @return true if drop was successful
	 */
	bool tryToDropCards(Stack& stack);
	/**
	 * @brief Searches for complete set of cards: { K, Q, J, 10, ..., 2, A }
	 * @param stack stack to search in
	 * @details function will automatically move the set to appropriate foundation and check win condition
	 */
	void searchForCompleteSet(Stack& stack);
	/**
	 * @brief Moves back all cards from hand to their previous location
	 */
	void rollback();
	/**
	 * @brief checks win condition and initializes win animation
	 */
	void checkWin(); // TODO refactor and add const
};

#endif /* SPIDER_H_ */
