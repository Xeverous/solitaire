#include "Game.hpp"

Game::Game()
	: mouse_moving_left(false), mouse_moving_right(false)
{
	// TODO remove duplicated code
	action_map["lpress"]   = thor::Action(sf::Mouse::Left,  thor::Action::PressOnce);
	action_map["rpress"]   = thor::Action(sf::Mouse::Right, thor::Action::PressOnce);
	action_map["move"]     = thor::Action(sf::Event::MouseMoved);
	action_map["lrelease"] = thor::Action(sf::Mouse::Left,  thor::Action::ReleaseOnce);
	action_map["rrelease"] = thor::Action(sf::Mouse::Right, thor::Action::ReleaseOnce);

	system.connect("lpress",   std::bind(Game::mousePressLeft, this));
	system.connect("rpress",   std::bind(Game::mousePressRight, this));
	system.connect("move",     std::bind(Game::mouseMove, this));
	system.connect("lrelease", std::bind(Game::mouseReleaseLeft, this));
	system.connect("rrelease", std::bind(Game::mouseReleaseRight, this));
}

void Game::handleEventByActionMap(const sf::Event& event, thor::ActionMap<std::string>::CallbackSystem& system, sf::RenderWindow* window)
{
	action_map.clearEvents();
	action_map.pushEvent(event);
	action_map.invokeCallbacks(system, window);
}

void Game::mousePressLeft()
{
	mouse_moving_left = false;
}

void Game::mousePressRight()
{
	mouse_moving_right = false;
}

void Game::mouseMove()
{
	if (!mouse_moving_left and sf::Mouse::isButtonPressed(sf::Mouse::Left))
	{
		mouse_moving_left = true;
		mouseHoldStartLeft();
	}

	if (!mouse_moving_right and sf::Mouse::isButtonPressed(sf::Mouse::Right))
	{
		mouse_moving_right = true;
		mouseHoldStartRight();
	}
}

void Game::mouseReleaseLeft()
{
	if (mouse_moving_left)
	{
		mouse_moving_left = false;
		mouseHoldEndLeft();
	}
	else
	{
		mouseClickLeft();
	}
}

void Game::mouseReleaseRight()
{
	if (mouse_moving_right)
	{
		mouse_moving_right = false;
		mouseHoldEndRight();
	}
	else
	{
		mouseClickRight();
	}
}
