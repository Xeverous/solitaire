#include "Solitaire.hpp"

#include "../Constant.hpp"
#include "../Global.hpp"

Solitaire::Solitaire()
{
	hand.SetSliding(Stack::Slide::vertical);

	for (auto& stack : tableaux)
		stack.SetSliding(Stack::Slide::vertical);

	newGame();
}

Solitaire::~Solitaire()
{
	save();
}

void Solitaire::newGame()
{
	stock.clear();
	waste.clear();

	for (auto& stack : foundations)
		stack.clear();

	for (auto& stack : tableaux)
		stack.clear();

	hand.clear();
	hand.newDeckStandard52();
	hand.shuffle();

	// put cards on the tableaux in the triangle distribution
	for (unsigned j = 0; j < tableaux.size(); j++)
		for (unsigned i = 0; i <= j; i++)
			hand.moveCardTo(tableaux[j]);

	for (auto& stack : tableaux)
		stack.setTopCardVisibility(true);

	// put the remaining cards to the stock
	hand.moveAllCardsTo(stock);

	reinitRender();
}

void Solitaire::restart()
{

}

void Solitaire::save()
{

}

void Solitaire::load()
{

}

void Solitaire::reinitRender()
{
	recalculateScale(sf::Vector2u(tableaux.size(), 2), sf::Vector2i(0, tableaux.size() + Card::Value::K));

	stock.setPosition(generateStackPosition({0, 0}));
	waste.setPosition(generateStackPosition({1, 0}));

	for (unsigned i = 0; i < foundations.size(); i++)
		foundations[i].setPosition(generateStackPosition({3 + i, 0}));

	for (unsigned i = 0; i < tableaux.size(); i++)
		tableaux[i].setPosition(generateStackPosition({i, 1}));

	using render_settings::scale;

	hand.setScale(scale);
	stock.setScale(scale);
	waste.setScale(scale);

	for (auto& stack : foundations)
		stack.setScale(scale);

	for (auto& stack : tableaux)
		stack.setScale(scale);
}

void Solitaire::handleEvent(const sf::Event& event, sf::Vector2f mouse_position)
{
	if (!active)
		return;

	#ifndef NDEBUG // space key cheat to auto win in debug build
	if (event.type == sf::Event::KeyPressed and event.key.code == sf::Keyboard::Space)
	{
		for (unsigned i = 0; i < foundations.size(); i++)
		{
			foundations[i].clear();
			foundations[i].newDeckColorSet13(static_cast<Card::Color>(i));
		}

		win_animation.clear();
		win_animation.addCardsFromStacks(foundations.begin(), foundations.end());
		win_animation.start();
		won = true;
	}
	#endif

	if (!won)
	{
		if (!hand.empty())
			hand.setPositionGrab(mouse_position);

		stock.handleHighlight(mouse_position);
		waste.handleHighlight(mouse_position);

		for (auto& stack : tableaux)
			stack.handleHighlight(mouse_position);

		for (auto& stack : foundations)
			stack.handleHighlight(mouse_position);

		handleEventByActionMap(event, system, nullptr);
	}
}

void Solitaire::update(sf::Time time)
{
	if (!active)
		return;

	if (won)
	{
		win_animation.update(time);
	}
	else
	{
		stock.update(time);
		waste.update(time);

		for (auto& stack : foundations)
			stack.update(time);

		for (auto& stack : tableaux)
			stack.update(time);
	}
}

bool Solitaire::tryToDropOnTableaux(Stack& stack)
{
	if (stack.empty())
	{
		if (hand.front().getValue() != Card::Value::K)
			return false;
	}
	else
	{
		if (stack.back().getValue() != hand.front().getValue() + 1)
			return false;

		if (stack.back().isRed() == hand.front().isRed())
			return false;
	}

	hand.dropAllCardsTo(stack);
	return true;
}

void Solitaire::draw(sf::RenderWindow& window)
{
	if (!active)
		return;

	stock.draw(window);
	waste.draw(window);

	for (auto& stack : foundations)
		stack.draw(window);

	for (auto& stack : tableaux)
		stack.draw(window);

	if (!hand.empty()) // don't draw hand's rectangle
		hand.draw(window);

	if (won)
		win_animation.draw(window);
}

/* -------------------------------------------------------------------
 * 				PRIVATE FUNCTIONS
 */
bool Solitaire::checkWin() const
{
	for (auto& stack : foundations)
	{
		if (stack.empty())
			return false;
		else if (stack.back().getValue() != Card::Value::K)
			return false;
	}

	return true;
}

/* -------------------------------------------------------------------
 * 				MOUSE CLICKS
 */

void Solitaire::mouseClickLeft()
{
	if (stock.isMouseAboveBase())
	{
		if (stock.empty()) // TODO: 1 or 3 cards depending on settings (currently implemented only for 1)
		{
			waste.moveAllCardsTo(stock);
			stock.hideAllCards();
		}
		else
		{
			stock.moveCardTo(waste);
			waste.setTopCardVisibility(true);
		}

		return;
	}

	for (auto& stack : tableaux)
		if (stack.isCovered())
		{
			if (!stack.empty())
				stack.setTopCardVisibility(true);

			return;
		}
}

void Solitaire::mouseClickRight()
{
	if (waste.isCovered())
	{
		if (!waste.empty())
			tryToDropOnAllFoundations(waste);

		return;
	}

	for (auto& stack : tableaux)
		if (stack.isCovered())
		{
			if (stack.empty())
				return;

			if (stack.getTopCardVisibility())
				tryToDropOnAllFoundations(stack);

			return;
		}
}

void Solitaire::mouseHoldStartLeft()
{
	if (!hand.empty())
		return;

	if (tryToPickUpTopCard(waste))
	{
		last_pickup = &waste;
		return;
	}

	for (auto& stack : tableaux)
		if (stack.isCovered())
		{
			if (tryToPickUpAllHighlightedCards(stack))
				last_pickup = &stack;

			return;
		}

	for (auto& stack : foundations)
		if (tryToPickUpTopCard(stack))
		{
			last_pickup = &stack;
			return;
		}
}

void Solitaire::mouseHoldEndLeft()
{
	if (hand.empty()) // nothing to drop
	{
		mouseClickLeft(); // QoL: if move did nothing, act as click happened
		return;
	}

	for (auto& stack : foundations)
		if (stack.isCovered())
		{
			if (hand.size() != 1)
				break;

			if (tryToDropOnFoundation(hand, stack))
				return;

			break;
		}

	for (auto& stack : tableaux)
		if (stack.isCovered())
		{
			if (tryToDropOnTableaux(stack))
				return;

			break;
		}

	rollback(); // flow will reach here if nothing was found to drop to
}

void Solitaire::mouseHoldEndRight()
{
	mouseClickRight(); // QoL feature: mouse accidental moves will not stop clicks
}

/* -------------------------------------------------------------------
 * 			TRYING FUNCTIONS
 * all of these returns false if the operation fails
 */
bool Solitaire::tryToPickUpTopCard(Stack& stack)
{
	if (!stack.isMouseAboveBase())
		return false;

	if (stack.empty())
		return false;

	if (!stack.getTopCardVisibility())
		return false;

	hand.pickCardFrom(stack);
	return true;
}

bool Solitaire::tryToPickUpAllHighlightedCards(Stack& stack)
{
	if (stack.empty())
		return false;

	if (!stack.areAllHighlightedCardsVisible())
		return false;

	const unsigned picked = stack.getHighlightedCardsAmount();
	for (unsigned i = 0; i < picked; i++)
		hand.pickCardFrom(stack);

	return true;
}

bool Solitaire::tryToDropOnFoundation(Stack& origin, Stack& foundation)
{
	if (foundation.empty())
	{
		if (origin.back().getValue() != Card::Value::AceLow)
			return false;

		origin.moveCardTo(foundation);
		return true;
	}

	if (origin.back().getValue() != foundation.back().getValue() + 1)
		return false;

	if (origin.back().getColor() != foundation.back().getColor())
		return false;

	origin.moveCardTo(foundation);
	won = checkWin();

	if (won)
	{
		win_animation.clear();
		win_animation.addCardsFromStacks(foundations.begin(), foundations.end());
		win_animation.start();
	}

	return true;
}

bool Solitaire::tryToDropOnAllFoundations(Stack& origin)
{
	for (auto& f : foundations)
		if (tryToDropOnFoundation(origin, f))
			return true;

	return false;
}

void Solitaire::rollback()
{
	hand.dropAllCardsTo(*last_pickup);
}
