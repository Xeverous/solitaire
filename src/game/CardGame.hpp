#ifndef CARDGAME_H
#define CARDGAME_H
#include <SFML/Graphics.hpp>
#include "Game.hpp"
#include "Stack.hpp"
#include "WinAnimation.hpp"

class CardGame : public Game
{
protected:
	bool won;
	bool active;
	WinAnimation win_animation;

	void setActive(bool status);

	void mouseClickLeft() override;
	void mouseClickRight() override;
	void mouseHoldStartLeft() override;
	void mouseHoldStartRight() override;
	void mouseHoldEndLeft() override;
	void mouseHoldEndRight() override;

	/**
	 * @brief recalculates variables related to rendering used by generateStackPosition()
	 * @param max_stacks requested screen proportions, to fit X stacks horizontally and Y stacks vertically
	 * @param sum_slides additional slides that can occur on stacks (will increase given space)
	 * @details example: feeding the function with {5, 2}, {10, -5} will result in scalng that can fit:
	 * - 5 stacks horizontally, where last stack spans a slide of 10 cards
	 * - 5 stacks horizontally, where each stack spans a slide of 2 cards
	 * - 2 stacks vertically, where bottom one is placed higher on the screen by a slide of 5 cards
	 * @see generateStackPosition()
	 */
	void recalculateScale(sf::Vector2u max_stacks, sf::Vector2i sum_slides = sf::Vector2i());

	/**
	 * @brief Generate position for a stack
	 * @param stacks X, Y position of the stack (1 = width/height of 1 card)
	 * @param slides X, Y offset to the position (1 = width/height of 1 card slide)
	 * @return coords that should be passed to Stack::SetPosition()
	 * @details Function intended to be called to easily set stack's position
	 * @note passed parameter values should not exceed values passed to recalculateScale()
	 */
	static sf::Vector2f generateStackPosition(sf::Vector2u stacks, sf::Vector2i slides = sf::Vector2i());

public:
	static float CARD_SPACE_FACTOR;
	static float CARD_SLIDE_FACTOR;

	CardGame();
	virtual ~CardGame() = default;

	virtual void reinitRender() = 0;
	virtual void newGame() = 0;
	virtual void restart() = 0;
	virtual void save() = 0;
	virtual void load() = 0;

	virtual void pause();
	virtual void resume(); ///< @brief apart from changing active, also calls reinitRender()

	virtual void handleEvent(const sf::Event& event, sf::Vector2f mouse_position) = 0;
	virtual void update(sf::Time time) = 0;
	virtual void draw(sf::RenderWindow& window) = 0;

	enum class GameType
	{
		Solitaire,
		Spider,
		Makao
	};
};

#endif // CARDGAME_H
