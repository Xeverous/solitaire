#ifndef GLOBAL_H
#define GLOBAL_H
#include <SFML/Graphics.hpp>
#include <Thor/Resources.hpp>
#include <Thor/Input.hpp>
#include <random>

namespace global
{
	extern std::mt19937 mt;
	extern sf::Vector2u window_size;

	enum class Asset
	{
		background,
		card_front,
		card_back,
		card_joker,
		card_hover,
		stack_empty,

		max_assets
	};

	extern std::array<const std::string, static_cast<unsigned>(Asset::max_assets)> file_paths;
	extern thor::ResourceHolder<sf::Texture, Asset> resource_holder;

	const std::string& getAssetPath(Asset name);
}

namespace render_settings
{
	extern float scale;
	extern sf::Vector2f card_scaled_size;
	extern sf::Vector2f card_space;
	extern sf::Vector2f card_slide;
}

#endif // GLOBAL_H


















