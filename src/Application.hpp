#ifndef APPLICATION_H
#define APPLICATION_H
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <deque>
#include <memory>

#include "game/CardGame.hpp"
#include "LanguagePack.hpp"
#include "UserInterface.hpp"

class Application
{
public:
	Application();
	~Application();

	void createGame(CardGame::GameType game_type);
	/**
	 * @brief Get pointer to the game
	 * @return Pointer to derived active game or nullptr if no game is active
	 */
	CardGame* getGame() const;
	int run();

private:
	sf::RenderWindow window;
	sf::Texture background_texture;
	sf::Sprite background_sprite;
	sf::Font menu_font;

	std::unique_ptr<CardGame> game;
	UserInterface ui;
	LanguagePack lp;

	void update(sf::Time time);

	void initSettingConfig();
	void initLog();
	void initRender();
	void initAssets();
	void gameLoop();

	void handleEvent(const sf::Event& event);
	void draw();
};

#endif // APPLICATION_H
