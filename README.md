# Solitaire #

Implementation of classic card games using libraries: [SFML](https://www.sfml-dev.org/index.php) and [SFGUI](http://sfgui.sfml-dev.de/) (based on OpenGL) in C++14 standard.

Currently - Solitaire and Spider (with planned improvements for aesthetics)

coming soon:

- Makao (with an AI)

- Doxygen support in comments

- support for multiple languages (mb add an XML/data library?)
